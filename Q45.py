'''45.Write a program to check given string is Palindrome or not.
 ( Use function Concepts and Required keyword, Default parameter concepts) That is reverse the given string and 
 check whether it is same as original string, if so then it is palindrome. 
 Example : String = "Malayalam" reverse string = "Malayalam" hence given string is palindrome.'''
def PalindromeCheckRequiredParam(myStr):
    #myStr="Hello buddy"
    myStr=myStr.lower()
    myStr1=myStr[::-1]
    if myStr==myStr1:
        print("The given string",myStr,"is palindrome")
    else:
        print("The given string",myStr,"is not palindrome")
    return

def PalindromeCheckDefaultParam(myStr="Malayalam"):
    'this checks for the palindrokme string'
    #myStr="Hello buddy"
    myStr=myStr.lower()
    myStr1=myStr[::-1]
    if myStr==myStr1:
        print("The given string",myStr,"is palindrome")
    else:
        print("The given string",myStr,"is not palindrome")
    return


PalindromeCheckRequiredParam("hello world")
PalindromeCheckRequiredParam("naman")


#myStr=input("Enter the string")
PalindromeCheckDefaultParam("naman")
PalindromeCheckDefaultParam()


