'''51. In a given directory search all text files for the pattern "Treasure". 
a) Find how many text file has the pattern.
b) Count how many times pattern repeats in each file
Note : Create at least 4 text file in a directory and keep the pattern in at least 2 files.
       Repeat the pattern in the file many times.'''

import glob
totalCount = 0
for eachfile in glob.glob("*.txt"):
    file = open(eachfile, "r")
    ls = file.read().split()
    if "Treasure" in ls:
        print("File name:",eachfile,"\nCount of letter Treasure in this file:", ls.count("Treasure"))
        totalCount += ls.count("Treasure")
    else:
        print("File name:",eachfile,"\nCount of letter Treasure in this file:", 0)
    file.close()        
print("Total counts of letter Treasure in all the files:", totalCount)

