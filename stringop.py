#stringop.py
def sortNumbers(num):
  for i in range(len(num)-1):
    for j in range(len(num)-1):
      if num[j] > num[j+1]:
        temp = num[j]
        num[j] = num[j+1]
        num[j+1] = temp
  print("Sorted numbers:", num)
    
def binarySearch(list, item):
    list.sort()
    first = 0
    last = len(list)-1
    found = False
    
    while first<=last and not found:
        midpoint = (first+last)//2
        if list[midpoint] == item:
            found = True
            return found
        else:
            if item < list[midpoint]:
                last = midpoint-1
            else:
                first = midpoint+1
    return found

def reverselist(list):
    list.reverse()
    print("reversed list :", list)
