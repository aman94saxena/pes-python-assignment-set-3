
'''55. Write a program for converting weight from Pound to Kilo grams.
       a) Use assertion for the negative weight.
       b) Use assertion to weight more than 100 KG
'''
       
def PoundToKg(pound):
  try:
    assert (pound>=0),"Negative weight not allowed"
    return pound*0.453592
  except AssertionError as exp:
    return exp
print(PoundToKg(25))
print(PoundToKg(-10))


def PoundToKg(pound):
  try:
    assert (pound>=100), "Weight should be more than or equal to 100"
    return pound*0.453592
  except AssertionError as exp:
    return exp
print(PoundToKg(23))
print(PoundToKg(120))
