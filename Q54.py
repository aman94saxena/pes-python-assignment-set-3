'''54. Write a program to handle the following exceptions in you program.
a) KeyboardInterrupt,
b) NameError 
c) ArithmeticError
Note : make use of  Try, except, else: statements.'''

import time
i=1
try:
    while(i<15):
        time.sleep(1)
        print(i)
        print("Please press 'Ctrl+C'")
        i+=1
except KeyboardInterrupt:
    print("KeyboardInterrupt")
    
    
try:
    name = input("Enter your name:")
    print("Good Morning "+ name)
except NameError:
    print("NameError")



try:
    num= 0/0
except ArithmeticError:
    print("ArithmeticError")
    

