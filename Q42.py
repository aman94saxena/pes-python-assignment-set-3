'''42.Write a program to generate a Fibonacci series using a function called fib(n),
 a) where N is user specified argument specifies number of elements in the series.'''
def fib(nterms):
    a=0
    b=1
    if nterms<=0:
        print("enter a positive number")
    elif nterms==1:
        print("Fibonacci series:",a)
    elif nterms==2:
        print(a)
        print(b)
    else:
        print(a)
        print(b)
        while(nterms>2):
            numnext=a+b
            print(numnext)
            a=b
            b=numnext
            
            nterms=nterms-1

n=int(input("Enter the no of terms"))
fib(n)