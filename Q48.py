
'''48.Create a Calculator with the following functions. 
a) Addition/subtraction/multiplication and division of two numbers ( Note: Create separate function for each operation ) 
b) Find square root of a given number.( Use keyword arguments in your function) 
c) Create a list of sub strings from a given string, such that sub strings are created with given character. That is, string = "Pack: My: Box: With: Good: Food" Create sub strings with the delimiter character ":" such that the following sub strings are created. substrlist=[Pack, My, Box, With, Good, Food] Note : Function should take at least 2 parameters ( Main string and delimiter character) return value from function will be list of substring.'''
import math
def Addition(a,b):
    sum=a+b
    print("sum is",sum)
def Subtraction(a,b):
    sub=a-b
    print("Subtraction is",sub)
def Multiply(a,b):
    mul=a*b
    print("multiplication is",mul)
def Division(a,b):
    div=a/b
    print("Division is",div)
def Sqrt(num):
    print("SQrt is ",math.sqrt(num))

Addition(3,2)
Subtraction(5,2)
Multiply(4,5)
Division(8,4)
Sqrt(16)
Sqrt(num=100)

def subStr(str,delcharctr):
    subStr= str.split(delcharctr,6)
    print("Original String :",str)
    return subStr
str="Pack:My:Box:With:Good:Food"
delcharctr=':'
subStr= subStr(str,delcharctr)
print("Substring from string is",subStr)


