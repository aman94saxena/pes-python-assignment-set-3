'''53. Open the file1 is read & write mode and apply following  functions
         a) All 13 functions mentioned in Tutorial file1 object table.'''

fo = open("file1.txt", "r+")
print("Name of the file1: ", fo.name)
# Close opend file1
fo.close()


fo = open("file1.txt", "r")
print("Name of the file1: ", fo.name)
fo.flush()
fo.close()

fo = open("file1.txt", "r+")
print("Name of the file1: ", fo.name)
fid = fo.fileno()
print("file1 Descriptor: ", fid)
fo.close()

fo = open("file1.txt", "r+")
print("Name of the file:", fo.name)
ret = fo.isatty()
print("Return value : ", ret)
fo.close()

fo = open("file1.txt", "r+")
print("Name of the file: ", fo.name)
for index in range(1):
   line = fo.readline()
   print("Line No %d - %s" % (index, line))
fo.close()

fo = open("file1.txt", "r+")
print("Name of the file: ", fo.name)
line = fo.read(100)
print("Read Line: %s" % (line))
fo.close()

fo = open("file1.txt", "r+")
print("Name of the file: ", fo.name)
line = fo.readline()
print("Read Line: %s" % (line))
line = fo.readline(5)
print("Read Line: %s" % (line))
fo.close()

fo = open("file1.txt", "r+")
print("Name of the file: ", fo.name)
line = fo.readlines()
print("Read Line: %s" % (line))
line = fo.readlines(2)
print("Read Line: %s" % (line))
fo.close()

fo = open("file1.txt", "r")
print("Name of the file: ", fo.name)
line = fo.readline()
print("Read Line: %s" % (line))
# Again set the pointer to the beginning
fo.seek(0, 0)
line = fo.readline()
print("Read Line: %s" % (line))
fo.close()

fo = open("file1.txt", "r+")
print("Name of the file: ", fo.name)
line = fo.readline()
print("Read Line: %s" % (line))
# Get the current position of the file1.
pos = fo.tell()
print("Current Position: %d" % (pos))
# Close opend file1
fo.close()


fo = open("file1.txt", "r+")
print("Name of the file: ", fo.name)
line = fo.readline()
print("Read Line: %s" % (line))
# Now truncate remaining file1.
fo.truncate()
# Try to read file1 now
line = fo.readline()
print("Read Line: %s" % (line))
fo.close()

fo = open("file1.txt", "w+")
print("Name of the file: ", fo.name)
str = "This is 6th line"
# Write a line at the end of the file1.
#fo.seek(0, 1)
line = fo.write( str )
# Now read complete file1 from beginning.
fo.seek(0,0)
lines=fo.read()
print(lines)
fo.close()

fo = open("file1.txt", "w+")
print("Name of the file: ", fo.name)
str = "This is 6th line"
# Write a line at the end of the file1.
#fo.seek(0, 1)
line = fo.writelines( str )
# Now read complete file1 from beginning.
fo.seek(0,0)
lines=fo.read()
print(lines)
fo.close()

