'''56.Write a program to handle following exceptions using Try block.
a) IO Error while you try writing contents into the file that is opened in read mode only.
b) ValueError'''

try:
    myfile =open("file1.txt","r")
    print(myfile.read())
    myfile.write("Hello World!")
except IOError:
    print("Writing not allowed")

try:
    num = int(input("Enter value:"))
except ValueError:
    print("ValueError")
