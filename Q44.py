'''44.Write a program with lambda function to perform following. 
a) Perform all the operations of basic calculator ( add, sub, multiply, divide, modulus, floor division ).'''

add=lambda x,y:x+y
sub=lambda x,y:x-y
mul=lambda x,y:x*y
div=lambda x,y:x/y
mod=lambda x,y:x%y
fdiv=lambda x,y:x//y


# Now call all the functions
print("Value as Sum:",add(30,20))
print("Value as Sub:",sub(20,10))
print("Value as Mul:",mul(20,10))
print("Value as Div:",div(20.0,12.0))
print("Value as Mod:",mod(20,10))
print("Value as Fdiv:",fdiv(20,3))